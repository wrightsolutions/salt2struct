#!/bin/sh
# Report on the state / existence / permissions of typical salt system directories
# Tailored towards debian initially
printf "# 1.First checks are for dirs below /srv/\n"
vdir -dlp /srv/salt /srv/pillar 
printf "## Note: above /srv/pillar should be 750 or 2750 permissions!!\n"
printf "# 2.Second checks are for dirs below /var/\n"
vdir -dlp /var/cache/salt /var/log/salt
printf "# 3.Third checks are for config dirs in /etc/\n"
vdir -dlp /etc/salt/pki /etc/salt/minion.d

