#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
*     *     *     *     *  cmdline
-     -     -     -     -
|     |     |     |     |
|     |     |     |     +----- dow - Day of week (0-7)
|     |     |     +------- Month (1 - 12)
|     |     +--------- dom - Day of month (1 - 31)
|     +----------- Hour (0 - 23) or say every 2 hours in first 6 hours ( 0-6/2 )
+------------- Min (0 - 59) or say every 10 minutes ( */10 )

# Example 10 minute cron in say crontab or 'root' user next:
*/10 * * * * /usr/lib64/sa/sa1 1 1

Running this program with a different sample input (an every 5 minute example) shown next
echo '*/5 * * * * /usr/bin/false' | python ./cron2salt.py

# ['*/5', '*', '*', '*', '*', '/bin/false']
"""

import fileinput
from os import getenv as osgetenv

PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)

ASTER=chr(42)
DOTTY=chr(46)
DQ=chr(34)
SQ=chr(39)
TABBY=chr(9)
CRON_PRESENT_DOTTED = 'cron.present:'
DENT = chr(32)*2
DENTDASHSPACE = "{0}{0}-{0}".format(chr(32))

def cron_present(array_len,cr_array,verbosity=0,user='root'):
	if array_len < 6:
		# Sixth field is cmdline. No sixth field, no playtime
		return 0
	arr = []
	for item in cr_array:
		if ASTER in item:
			arr.append("{0}{1}{0}".format(SQ,item))
		else:
			arr.append(item)
	print(CRON_PRESENT_DOTTED)
	print("{0}user: {1}".format(DENTDASHSPACE,user))
	print("{0}minute: {1}".format(DENTDASHSPACE,arr[0]))
	print("{0}hour: {1}".format(DENTDASHSPACE,arr[1]))
	print("{0}name: {1}".format(DENTDASHSPACE,arr[5]))
	print("{0}identifier: {1}_{2}".format(DENTDASHSPACE,user,arr[5]))
	return array_len


for line in fileinput.input():
	ln_array = line.rstrip().split()
	ln_array_len = len(ln_array)
	if ln_array_len < 6:
		continue
	cron_present(ln_array_len,ln_array,PYVERBOSITY)
