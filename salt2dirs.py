#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2018 Gary Wright wrightsolutions.co.uk/contact
# 
# Redistribution and use in source and binary forms,
# with or without modification,
# are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation and/or
# other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor
# the names of its contributors may be used to endorse or promote
# products derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

""" Script to create two directory structure 'saltstack' and 'pillar'

Working directory is the current directory

First argument is the name of the 'module' or 'subjectarea'
that you are automating. Example: postgres

cd /data/saltstack/    # Here we are changing directory to the root of where we want sls modules to go
python ./salt2dirs.py 'postgres'

The pillar directory is created in /tmp/ and should be moved immediately into /srv/salt/base/pillar/ or similar
if you are planning to use the file tree generated for pillar
"""

#import re
import os
from os import linesep as linesep
from sys import argv
#from string import printable

SPACES2 = chr(32)*2
SPACES4 = chr(32)*4
SPACES6 = chr(32)*6
SPACES8 = chr(32)*8
ASTER=chr(42)
HASHER=chr(35)
PERC=chr(37)
DENT = chr(32)*2
DENTDASHSPACE = "{0}{0}-{0}".format(chr(32))
PARENTH_OPEN = chr(40)
PARENTH_CLOSE = chr(41)
# Above are () and next we define {}
BRACES_OPEN = chr(123)
BRACES_CLOSE = chr(125)
JIN_OPEN = "{0}{1}".format(BRACES_OPEN,PERC)
JIN_CLOSE = "{0}{1}".format(PERC,BRACES_CLOSE)
JINCOMMENT_OPEN = "{0}{1}{1}".format(BRACES_OPEN,HASHER)
JINCOMMENT_CLOSE = "{0}{0}{1}".format(HASHER,BRACES_CLOSE)
#PIPE_BACKSLASH = "{0} {1}".format(chr(124),chr(92))
#RE_PARENTHESIZED_WORD = re.compile(r'\(\w+\)')


CODING_TEMPLATE="""{0} -{1}- coding: utf-8 -{1}-{2}{0} vim: ft=yaml{2}"""
CODING_PLUS=CODING_TEMPLATE.format(HASHER,ASTER,linesep)

DEFAULTS_YAML="""{1}
maria:
  databases:
    - invacct
  dbusers:
{0}invoices:
{0}  user: invadmin
{0}  password: invadmin123
{0}  hosts:
{0}{0}- localhost
{0}{0}- '%.cluster.local'
{0}{0}- 'inv01.cluster.local'
{0}{0}- 'inv02.cluster.local'
""".format(SPACES4,CODING_PLUS)


DEFAULTS_MERGER_TEMPLATE="""{6}
{4} Start with defaults from defaults.yaml file in this directory {5}
{2} import_yaml '{7}/defaults.yaml' as default_settings {3}

{4}
Setup variable using grains['os'] based logic.
Only add key values pairs which differ from that in defaults.yaml
{5}
{2} set os_map = salt['grains.filter_by']({0}
    'Debian': {0}
    {1},
  {1}
  , grain="os"
  , merge=salt['pillar.get']('{7}'))
{3}
{4} Merge the os_map to the default settings {5}
{2} do default_settings.{7}.update(os_map) {3}

{4} Pillar for {7} is now merged with what have so far {5}
{2} set {7} = salt['pillar.get'](
        '{7}',
        default=default_settings.{7},
        merge=True
    )
{3}
"""

INIT_SLS_TEMPLATE="""{6}
{2} from "{7}/map.jinja" import {7} with context {3}

include:
  - {7}.packages_service
#  - {7}.sudoers_plus
"""

MAIN_SLS_FIRST_TEMPLATE="""{6}
{2} from "{7}/map.jinja" import {7} with context {3}

include:
  - repos.epel
  - users

nrpe:
  pkg.installed:
    - pkgs:
      - nrpe
      - nagios-plugins-disk
    - refresh: True
    - require:
      - pkgrepo: epel.repo
      - user: nrpe.user
      - group: nrpe.group
  service.running:
    - name: nrpe
    - enable: True
    - watch:
      - file: /etc/nagios/nrpe.cfg
    - require:
      - pkg: nrpe
      - user: nrpe.user
      - group: nrpe.group
"""

MAIN_SLS_SECOND_TEMPLATE="""{6}
{2} from "{7}/map.jinja" import {7} with context {3}

include:
  - {7}

nrpe_sudoers:
  file.managed:
    - name: /etc/sudoers.d/nrpe
    - source: salt://{7}/files/sudoers_nrpe
    - mode: 644

nagios64plugins:
  file.recurse:
    - name: /usr/lib64/nagios/plugins
    - source: salt://{7}/files/plugins
    - file_mode: 755
"""

MODPILLAR_FIREWALL="""{1}
firewall:
  staticfile: true
  services:
    firewalld:
      enabled: False
    iptables:
      enabled: False

  headnoderange: 10.3.160.1/32
  loginrange: 10.3.160.12/29
  nfsrange: 10.3.160.20/30

  ports:
{0}22: 0.0.0.0/0
{0}53: 0.0.0.0/0
""".format(SPACES4,CODING_PLUS)

MODPILLAR_SPECIFIC_TEMPLATE="""{2}
{0}7{1}:
{3}homedir: '/mnt/nfs/home'
{3}nss_entry_cache_nowait_percentage: 25
{3}permdefault: 755
{3}default_gid: 100008
{4} mounts:
{4}   storage:
{4}     range4masked: aa.bb.cc.0/22
""".format(BRACES_OPEN,BRACES_CLOSE,CODING_PLUS,SPACES2,HASHER)


MODPILLAR_TOP_TEMPLATE="""{2}
# 'vertical' is a placeholder for an application stack or other vertical implementation
base:
  '*':
{3}{4}- desktop
{3}{4}- server
{3}- {0}7{1}
{3}{4}- vertical
""".format(BRACES_OPEN,BRACES_CLOSE,CODING_PLUS,SPACES4,HASHER)


def makedir_tilde_expand(path_short,failure_message=''):
	path_expanded = None
	if path_short is None or len(path_short) < 2:
		path_expanded = os.getcwd()

	if not path_expanded:
		# Still work to do
		if path_short.startswith('~/'):
			path_expanded = path_short.replace('~/',"{0}/".format(expandres),1)
		elif path_short.startswith('./'):
			path_expanded = path_short.replace('./',"{0}/".format(os.getcwd()),1)
		else:
			path_expanded = path_short

	failure_count = None

	if path_expanded:

		failure_count = 0
		failure_key = ''
		# Above set blank initially

		try:
			#os.makedirs(path_expanded, mode=0o775)
			""" makedirs() will create intermediate directories
			if required so we use that fact to ensure we have
			an empty 'files' directory below
			the main (named) directory for sls
			"""
			os.makedirs(path_expanded + os.sep + 'files', mode=0o775)
			#os.makedirs(path_expanded + os.sep + 'saltstack', mode=0o775)
		except OSError:
			failure_key = 'givenexists'
		except:
			failure_key = 'given'

		if len(failure_key) > 0:
			failure_count += 1
			if len(failure_message) > 0:
				print("Makedir at {0} failed - {1} ( {2} )".format(path_short,
									failure_message,failure_key))
			failure_key = ''

		try:
			os.makedirs(os.sep + 'tmp' + os.sep + 'pillar', mode=0o750)
		except OSError:
			failure_key = 'pillarexists'
		except:
			failure_key = 'pillar'

		if len(failure_key) > 0:
			failure_count += 1
			if len(failure_message) > 0:
				if failure_key.startswith('pillar'):
					failure_stem = "Making directory for pillar in /tmp/ failed"
				else:
					failure_stem = "Makedir at {0} failed".format(path_short)
				print("{0} - {1} ( {2} )".format(failure_stem,failure_message,
										failure_key))

	makedir_res = False
	# No makedir attempt or any failure results in return of False
	if 0==failure_count:
		makedir_res = True

	return makedir_res


def textfile_writer(textfile_path,string_multi,writemode='wt'):
	written_count = 0

	lines_unstripped = string_multi.split(linesep)
	lines = []
	for ln in lines_unstripped:
		line = ln.rstrip()
		if len(line) > 0:
			lines.append(line)
			continue
		if len(lines) < 1:
			continue
		""" The 'continue' immdiately above prevents
		blank lines at start of the stream/file from
		fooling us into thinking there is genuine content
		"""
		lines.append('')

	if len(lines) < 1:
		return 0
	""" If writemode is 'x' we fail to write if file exists
	but 'x' is new to Python 3
	"""
	with open(textfile_path, writemode) as outf:
		for line in lines:
			outf.write("{0}{1}".format(line,linesep))
			written_count += 1

	return written_count


if __name__ == '__main__':

	program_binary = argv[0].strip()

	modname_string = ''
	if len(argv) > 1:
		modname_string = argv[1].strip()

	if len(modname_string) < 2:
		exit(101)

	program_binary_basename = os.path.basename(program_binary)

	exit_rc = 0
	modname_relative = ".{0}{1}".format(os.sep,modname_string)
	makedir_res = makedir_tilde_expand(modname_relative,
			'Unable to make boilerplate dirs')
	if makedir_res:
		pass
	else:
		exit_rc = 121

	# dir_util.copy_tree(static_dir, output_dir)

	mainfiles = 0
	main_defaults_fullpathed = ".{0}{1}{0}{2}".format(os.sep,
						modname_string,'defaults.yaml')
	main_written_linecount = textfile_writer(main_defaults_fullpathed,DEFAULTS_YAML)
	if main_written_linecount > 0:
		mainfiles += 1
	exit_rc = mainfiles + 130
	""" 130 for exit_rc is certainly bad
	as means we have managed to write ZERO mainfiles
	"""

	format_vals8 = (BRACES_OPEN,BRACES_CLOSE,JIN_OPEN,JIN_CLOSE,
		JINCOMMENT_OPEN,JINCOMMENT_CLOSE,CODING_PLUS,modname_string)

	try:
		DEFAULTS_MERGER = DEFAULTS_MERGER_TEMPLATE.format(*format_vals8)
	except IndexError:
		DEFAULTS_MERGER = ''
		raise
	except KeyError:
		DEFAULTS_MERGER = ''
		raise
	except ValueError:
		DEFAULTS_MERGER = ''
		raise

	main_merger_fullpathed = ".{0}{1}{0}{2}".format(os.sep,
						modname_string,'map.jinja')

	writer_res = textfile_writer(main_merger_fullpathed,DEFAULTS_MERGER)
	if writer_res > 0:
		mainfiles += 1
		main_written_linecount += writer_res
	exit_rc = mainfiles + 130

	try:
		MAIN_INIT_SLS = INIT_SLS_TEMPLATE.format(*format_vals8)
	except IndexError:
		MAIN_INIT_SLS = ''
		raise
	except KeyError:
		MAIN_INIT_SLS = ''
		raise
	except ValueError:
		MAIN_INIT_SLS = ''
		raise

	main_init_fullpathed = ".{0}{1}{0}{2}".format(os.sep,
						modname_string,'init.sls')

	writer_res = textfile_writer(main_init_fullpathed,MAIN_INIT_SLS)
	if writer_res > 0:
		mainfiles += 1
		main_written_linecount += writer_res
	exit_rc = mainfiles + 130


	try:
		MAIN_SLS1 = MAIN_SLS_FIRST_TEMPLATE.format(*format_vals8)
	except IndexError:
		MAIN_SLS1 = ''
		raise
	except KeyError:
		MAIN_SLS1 = ''
		raise
	except ValueError:
		MAIN_SLS1 = ''
		raise

	main_sls1_fullpathed = ".{0}{1}{0}{2}".format(os.sep,
						modname_string,'packages_service.sls')

	writer_res = textfile_writer(main_sls1_fullpathed,MAIN_SLS1)
	if writer_res > 0:
		mainfiles += 1
		main_written_linecount += writer_res
	exit_rc = mainfiles + 130

	try:
		MAIN_SLS2 = MAIN_SLS_SECOND_TEMPLATE.format(*format_vals8)
	except IndexError:
		MAIN_SLS2 = ''
		raise
	except KeyError:
		MAIN_SLS2 = ''
		raise
	except ValueError:
		MAIN_SLS2 = ''
		raise

	main_sls1_fullpathed = ".{0}{1}{0}{2}".format(os.sep,
						modname_string,'sudoers_plus.sls')

	writer_res = textfile_writer(main_sls1_fullpathed,MAIN_SLS2)
	if writer_res > 0:
		mainfiles += 1
		main_written_linecount += writer_res
		exit_rc = 130
	else:
		exit_rc = mainfiles + 130

	if exit_rc > 130:
		exit(exit_rc)

	""" Having completed writing files to main mod named area
	we now move on to working on the pillar
	"""

	try:
		PILLAR_SLS1 = MODPILLAR_FIREWALL.format(*format_vals8)
	except IndexError:
		PILLAR_SLS1 = ''
		raise
	except KeyError:
		PILLAR_SLS1 = ''
		raise
	except ValueError:
		PILLAR_SLS1 = ''
		raise

	#os.makedirs(os.sep + 'tmp' + os.sep + 'pillar', mode=0o750)
	pillar_sls1_fullpathed = "{0}tmp{0}pillar{0}{2}".format(os.sep,
						modname_string,'firewall.sls')

	pfiles = 0
	writer_res = textfile_writer(pillar_sls1_fullpathed,PILLAR_SLS1)
	if writer_res > 0:
		pfiles += 1
		pillar_written_linecount = writer_res
	exit_rc = pfiles + 140

	try:
		PILLAR_SLS2 = MODPILLAR_SPECIFIC_TEMPLATE.format(*format_vals8)
	except IndexError:
		PILLAR_SLS2 = ''
		raise
	except KeyError:
		PILLAR_SLS2 = ''
		raise
	except ValueError:
		PILLAR_SLS2 = ''
		raise

	pillar_sls2_fullpathed = "{0}tmp{0}pillar{0}{1}.sls".format(os.sep,
						modname_string)

	writer_res = textfile_writer(pillar_sls2_fullpathed,PILLAR_SLS2)
	if writer_res > 0:
		pfiles += 1
		pillar_written_linecount += writer_res
	else:
		exit_rc = pfiles + 140

	try:
		PILLAR_SLS0 = MODPILLAR_TOP_TEMPLATE.format(*format_vals8)
	except IndexError:
		PILLAR_SLS0 = ''
		raise
	except KeyError:
		PILLAR_SLS0 = ''
		raise
	except ValueError:
		PILLAR_SLS0 = ''
		raise

	pillar_sls0_fullpathed = "{0}tmp{0}pillar{0}top.sls".format(os.sep,
						modname_string)

	writer_res = textfile_writer(pillar_sls0_fullpathed,PILLAR_SLS0)
	if writer_res > 0:
		pfiles += 1
		pillar_written_linecount += writer_res
	else:
		exit_rc = pfiles + 140

	#print((mainfiles,pfiles))
	if 5==mainfiles and 3==pfiles:
		exit_rc = 0

	exit(exit_rc)

