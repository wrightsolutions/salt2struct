#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2018,2016, Gary Wright wrightsolutions.co.uk/contact
# 
# Redistribution and use in source and binary forms,
# with or without modification,
# are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation and/or
# other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor
# the names of its contributors may be used to endorse or promote
# products derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

""" Script to create a file.managed section of an input configuration file

First argument is section_label and will be the name that is given to the section in salt

./salt_file_managed.py 'collectd_thresholds' thresholds.conf
"""

import fileinput
import re
#from os import path,sep
from sys import argv
#from string import printable
prog = argv[0].strip()

SPACES2 = chr(32)*2
SPACES6 = chr(32)*6
SPACES8 = chr(32)*8


TEMPLATE_BEFORE="""%s:
  file.managed:
  - name: /tmp/%s
  - contents: |"""


def content_indented(fileinput_given):
    lines_indented = []
    for line in fileinput.input(fileinput_given):
        lines_indented.append("%s%s" % (SPACES6,line))
    return lines_indented


if __name__ == '__main__':
    exit_rc = 0

    section_label = None
    
    if len(argv) > 1:
        section_label = argv[1]
    else:
        section_label = 'salt_managed1'

    fileinput_given = argv[2:]
    section_lines = content_indented(fileinput_given)
    
    if len(section_lines) < 1:
        exit_rc = 151
    else:
        section_label_stripped = section_label.strip()
        managed_filename = fileinput_given[0]
        if fileinput_given[0].startswith(chr(47)):
            managed_filename = managed_filename.lstrip(chr(47))
        print TEMPLATE_BEFORE % (section_label_stripped,managed_filename)
        for line in section_lines:
            print line,
        print "%s- backup: minion" % SPACES2
    exit(exit_rc)

